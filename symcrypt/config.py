import os


class Config(object):
    APP_DEBUG = os.getenv("APP_DEBUG", True)
    API_VERSION = ["v1"]
    APP_NAME = "user_manager"
    MONGO_HOST = os.getenv("APP_DB_HOST", '127.0.0.1')
    MONGO_PORT = os.getenv("APP_DB_PORT", '27017')
    MONGO_DB_NAME = os.getenv("APP_DB_NAME", 'localhost')
    MONGO_DB_USER = os.getenv("APP_USERNAME", 'comrade')
    MONGO_URI = os.getenv("MONGO_URI", "mongodb://{url}:{port}/{dbname}?connect=false"
                          .format(url=MONGO_HOST, port=MONGO_PORT, dbname=MONGO_DB_NAME))
    REDIS_HOST = os.getenv("REDIS_HOST", '127.0.0.1')
    REDIS_PORT = os.getenv("REDIS_PORT", '6379')
    REDIS_INDEX = os.getenv("REDIS_INDEX", '1')
    KAVENEGAR_API_KEY = os.getenv("KAVENEGAR_API_KEY",
                                  "3837426B4C2B54594A666F56702F31564F794C587A476B63576E773766617439")
    VENTURES_BASE_URL = os.getenv("VENTURES_BASE_URL", "http://127.0.0.1:6969/v1")
    INTERNAL_AUTH_TOKEN = os.getenv("INTERNAL_AUTH_TOKEN", '')


# API Config
SECRET_KEY = 'Something Secret'

# Modules
INSTALLED_MODULES = [
    'user',
    'crypto',
    # 'price'
]
