from marshmallow import Schema, fields


# to validate data before inserting into db
class CryptoPriceModel(Schema):
    name = fields.String()
    desc = fields.String()
    price = fields.Integer()
