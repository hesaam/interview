from flask_restful import Resource, marshal
from flask import request, make_response, jsonify
from flask_login import login_required
from symcrypt.logics.db import get_history


class PriceResource(Resource):
    '''
    Provide an the basic price and an aggregation

        input:  /price/?name_of_the_currency

    '''

    @staticmethod
    @login_required
    def get(self):
        c_name = request.args.get("currency_name")
        if not c_name:
            return make_response("you need specify a crypto currency ", 400)

        resp = get_history(c_name)
        if not resp:
            return make_response("currency not found", 400)
        return make_response(jsonify(resp))

