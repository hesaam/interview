from pymongo import MongoClient
from symcrypt.config import Config


def mongo_connection(db_name):
    _db = MongoClient("mongodb://{db_host}:{db_port}/{db_name}?connect=false" \
                      .format(db_host=Config.MONGO_HOST, db_port=Config.MONGO_PORT, db_name=db_name))
    return _db
