from symcrypt.helpers.mongo import mongo_connection


def get_history(currency_name: str):
    _client = mongo_connection("currency")
    db = _client.currency
    collection = db.history
    query = {"name": currency_name}  # TODO can be aggregated with a better query
    history = collection.find(query)
    return history


def insert(data):
    if not data:
        return
    _client = mongo_connection("currency")
    db = _client.currency
    collection = db.history
    collection.insert(data)
    return
